package cl.hardroid.supermerk2.presentation.productos.viewmodel

data class ProductosViewModel(
                              val id: Long,
                              val name: String,
                              val price: String,
                              val fecha: String,
                              val categoria: String,
                              val proveedor: String)
