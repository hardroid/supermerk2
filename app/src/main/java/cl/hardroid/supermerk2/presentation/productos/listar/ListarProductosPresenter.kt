package cl.hardroid.supermerk2.presentation.productos.listar

import cl.hardroid.supermerk2.domain.entity.ProductoEntity
import cl.hardroid.supermerk2.domain.usecase.GetProductUseCase
import cl.hardroid.supermerk2.presentation.base.AbstractPresenter
import cl.hardroid.supermerk2.presentation.base.viewmodel.ErrorViewModel
import cl.hardroid.supermerk2.presentation.productos.mapper.ProductosEntityToProductoViewModelMapper
import cl.hardroid.supermerk2.presentation.productos.viewmodel.ProductosViewModel
import io.reactivex.observers.DisposableObserver
import javax.inject.Inject

class ListarProductosPresenter

@Inject
constructor() : AbstractPresenter<ListarProductoContract.View>(),
                ListarProductoContract.Presenter {

    @Inject lateinit var navigator: ListarProductoContract.Navigator

    @Inject lateinit var useCase: GetProductUseCase

    @Inject lateinit var mapper: ProductosEntityToProductoViewModelMapper


    override fun loadProducts() {
        view?.showProgresDialog()
        useCase.execute(ProductoDisposableObserver())
    }

    override fun viewDetails(producto: ProductosViewModel) {
        navigator?.goToDetails(producto)
    }

    override fun agregarProductoClick() {
        navigator.goToNewProduct()
    }

    private inner class ProductoDisposableObserver : DisposableObserver<List<ProductoEntity>>(){
        override fun onComplete() {

        }

        override fun onNext(t: List<ProductoEntity>) {
            if(t.isEmpty()){
                view?.showEmptyList()
            }else {
                view?.listProducts(mapper.mapAll(t))
            }
            view?.hideProgresDialog()
        }

        override fun onError(e: Throwable) {
            view?.hideProgresDialog()
            view?.showError(ErrorViewModel("Error", "${e.message}"))
        }
    }
}