package cl.hardroid.supermerk2.presentation.categoria.mapper

import cl.hardroid.supermerk2.domain.base.Mapper
import cl.hardroid.supermerk2.domain.entity.CategoriaEntity
import dagger.Reusable
import javax.inject.Inject

@Reusable
class CategoriaEntityToStringMapper
@Inject constructor() : Mapper<CategoriaEntity, String>() {

    override fun map(input: CategoriaEntity): String {
        return input.nombre
    }
}