package cl.hardroid.supermerk2.presentation.productos.listar

import cl.hardroid.supermerk2.presentation.base.MVPContract
import cl.hardroid.supermerk2.presentation.base.viewmodel.ErrorViewModel
import cl.hardroid.supermerk2.presentation.productos.viewmodel.ProductosViewModel

interface ListarProductoContract {
    interface View : MVPContract.BaseView{
        fun showProgresDialog()
        fun hideProgresDialog()
        fun showErrorNoInternetConnection()
        fun showError(error: ErrorViewModel)
        fun listProducts(products: List<ProductosViewModel>)
        fun viewDetailsProduct(producto: ProductosViewModel)
        fun showEmptyList()
    }

    interface Presenter : MVPContract.BasePresenter<View>{
        fun loadProducts()
        fun viewDetails(producto: ProductosViewModel)
        fun agregarProductoClick()
    }

    interface Navigator : MVPContract.Navigator{
        fun goToDetails(producto: ProductosViewModel)
        fun goToNewProduct()
    }
}