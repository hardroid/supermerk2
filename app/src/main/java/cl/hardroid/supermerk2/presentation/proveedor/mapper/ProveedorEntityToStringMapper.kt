package cl.hardroid.supermerk2.presentation.proveedor.mapper

import cl.hardroid.supermerk2.domain.base.Mapper
import cl.hardroid.supermerk2.domain.entity.ProveedorEntity
import javax.inject.Inject

class ProveedorEntityToStringMapper
@Inject constructor() : Mapper<ProveedorEntity, String>() {
    override fun map(input: ProveedorEntity): String {
        return input.nombre
    }
}