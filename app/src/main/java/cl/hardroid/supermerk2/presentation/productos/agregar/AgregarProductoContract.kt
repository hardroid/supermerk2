package cl.hardroid.supermerk2.presentation.productos.agregar

import cl.hardroid.supermerk2.presentation.base.MVPContract
import cl.hardroid.supermerk2.presentation.base.viewmodel.ErrorViewModel
import cl.hardroid.supermerk2.presentation.productos.viewmodel.ProductosViewModel

interface AgregarProductoContract {

    interface View : MVPContract.BaseView{
        fun showProgresDialog()
        fun hideProgresDialog()
        fun showErrorNoInternetConnection()
        fun showError(error: ErrorViewModel)
        fun showMessageIncompleteForm()
        fun cleanForm()
        fun showMessageRegisterOk()
        fun showDatePicker()
        fun obtieneCategorias(categorias: List<String>)
        fun obtieneProveedores(categorias: List<String>)
    }

    interface Presenter : MVPContract.BasePresenter<View>{
        fun validaDatos(producto: ProductosViewModel) : Boolean
        fun agregarProducto(producto: ProductosViewModel)
        fun buscaCategorias(filter: String)
        fun buscaProveedores(filter: String)
    }

    interface Navigator : MVPContract.Navigator{
        fun closePage()
    }
}