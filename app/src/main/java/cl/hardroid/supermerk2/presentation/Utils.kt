package cl.hardroid.supermerk2.presentation

import java.text.SimpleDateFormat
import java.util.*

class Utils {
    fun subtractDay(date: Date, day: Int): Date {
        val cal = Calendar.getInstance()
        cal.time = date
        cal.add(Calendar.DAY_OF_MONTH, day)
        return cal.time
    }

    fun getDateWithFormat(calendar: Calendar, format: String): String {
        val outputFormat = SimpleDateFormat(format)

        val cal = calendar.time

        return outputFormat.format(cal)
    }

    fun getDateActual(): String{
        return getDateWithFormat(Calendar.getInstance(),"dd-MM-yyyy")
    }
}