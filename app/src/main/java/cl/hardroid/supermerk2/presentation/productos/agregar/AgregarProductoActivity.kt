package cl.hardroid.supermerk2.presentation.productos.agregar

import android.app.DatePickerDialog
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.DatePicker
import cl.hardroid.supermerk2.R
import cl.hardroid.supermerk2.di.scope.ProductoScope
import cl.hardroid.supermerk2.presentation.Utils
import cl.hardroid.supermerk2.presentation.base.DialogActivity
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_ingresar_producto.*
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject
import android.widget.ArrayAdapter
import cl.hardroid.supermerk2.presentation.base.viewmodel.ErrorViewModel
import cl.hardroid.supermerk2.presentation.productos.viewmodel.ProductosViewModel


class AgregarProductoActivity : DialogActivity(), AgregarProductoContract.View, View.OnClickListener {

    @ProductoScope
    @Inject
    lateinit var navigator: AgregarProductoContract.Navigator

    @ProductoScope
    @Inject
    lateinit var presenter: AgregarProductoContract.Presenter


    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ingresar_producto)
        presenter.bindView(this)
        presenter.buscaCategorias("")
        presenter.buscaProveedores("")
        btnAgregarProducto.setOnClickListener(this)
        btnCambiarFecha.setOnClickListener(this)
        etFechaIngreso.setText(Utils().getDateActual())
    }

    override fun showMessageIncompleteForm() {
        showAlert(
                "Error",
                "Debe ingresar todos los campos",
                "Aceptar",
                object : OnClickOkEvents {
                    override fun onClickOk() {
                        dismiss()
                    }
                })
    }

    override fun showProgresDialog() {
        showProgressDialog(
                "Supermerk2",
                "Cargando...",
                false)
    }

    override fun hideProgresDialog() {
        dismiss()
    }

    override fun showErrorNoInternetConnection() {
        showAlert(
                "No hay Conexión",
                "Revise su conexión de Internet",
                "Aceptar",
                object : OnClickOkEvents {
                    override fun onClickOk() {
                        dismiss()
                    }
                })
    }

    override fun showError(error: ErrorViewModel) {
        showAlert(
                error.title,
                error.message,
                "Aceptar",
                object : OnClickOkEvents {
                    override fun onClickOk() {
                        dismiss()
                    }
                })
    }

    override fun showMessageRegisterOk() {
        showAlert("Supermerk2",
                "Registro guardado OK :)",
                "OK",
                object : OnClickOkEvents {
                    override fun onClickOk() {
                        dismiss()
                    }
                })
    }

    override fun cleanForm() {
        etCategoria.text.clear()
        etFechaIngreso.setText(Utils().getDateActual())
        etName.text.clear()
        etPrice.text.clear()
        etProveedor.text.clear()
        etName.requestFocus()
    }

    override fun showDatePicker() {
        var cal = Calendar.getInstance()
        val dateSetListener = object : DatePickerDialog.OnDateSetListener {
            override fun onDateSet(view: DatePicker, year: Int, monthOfYear: Int,
                                   dayOfMonth: Int) {
                cal.set(Calendar.YEAR, year)
                cal.set(Calendar.MONTH, monthOfYear)
                cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                val myFormat = "dd-MM-yyyy" // mention the format you need
                val sdf = SimpleDateFormat(myFormat, Locale.getDefault())
                etFechaIngreso!!.setText(sdf.format(cal.getTime()))
            }
        }

        DatePickerDialog(this,
                dateSetListener,
                // set DatePickerDialog to point to today's date when it loads up
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH)).show()
    }

    override fun onClick(v: View?) {
        if (v!!.id.equals(R.id.btnCambiarFecha)) {
            showDatePicker()
        } else if (v!!.id.equals(R.id.btnAgregarProducto)) {
            presenter.agregarProducto(
                    ProductosViewModel(
                            0,
                            etName.text.toString(),
                            etPrice.text.toString(),
                            etFechaIngreso.text.toString(),
                            etCategoria.text.toString(),
                            etProveedor.text.toString()
                    )
            )
        }
    }

    override fun obtieneCategorias(categorias: List<String>) {
        val adapter = ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, categorias)
        etCategoria.setAdapter<ArrayAdapter<String>>(adapter)
    }

    override fun obtieneProveedores(proveedores: List<String>) {
        val adapter = ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, proveedores)
        etProveedor.setAdapter<ArrayAdapter<String>>(adapter)
    }
}
