package cl.hardroid.supermerk2.presentation.productos.agregar

interface OnAgregarProductoCallback {
    fun onAgregarProductoComplete(t: Boolean)
    fun onAgregarProductoError(e: Throwable)
}