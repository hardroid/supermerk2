package cl.hardroid.supermerk2.presentation.base

import android.content.DialogInterface
import android.support.v7.app.AppCompatActivity
import org.jetbrains.anko.alert
import org.jetbrains.anko.indeterminateProgressDialog

abstract class DialogActivity : AppCompatActivity() {

    lateinit var dialog : DialogInterface

    fun showAlert(titleValue: String, messageValue : String, botonOkText : String, botonCancelText : String, events : OnClickOkCancelEvents){
        dialog = alert {
            title = titleValue
            message = messageValue

            positiveButton(botonOkText){
                events.onClickOk()
                dialog.dismiss()
            }

            negativeButton(botonCancelText){
                events.onClickCancel()
                dialog.dismiss()
            }
            show()
        }.build()
    }

    fun showAlert(titleValue: String, messageValue : String, botonOkText : String, events : OnClickOkEvents){
        dialog = alert {
            title = titleValue
            message = messageValue

            positiveButton(botonOkText){
                events.onClickOk()
                dialog.dismiss()
            }
            show()

        }.build()
    }

    fun dismiss(){
        dialog.dismiss()
    }

    fun showProgressDialog(titleValue: String, messageValue: String, isCancelable : Boolean){
        dialog = indeterminateProgressDialog(messageValue, titleValue){
            setCancelable(isCancelable)
            show()
        }

    }

    interface OnClickCancelEvents{
        fun onClickCancel()
    }


    interface OnClickOkEvents{
        fun onClickOk()
    }

    interface OnClickOkCancelEvents : OnClickOkEvents{
        fun onClickCancel()
    }

}