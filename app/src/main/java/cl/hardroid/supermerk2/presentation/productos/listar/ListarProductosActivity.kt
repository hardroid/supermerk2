package cl.hardroid.supermerk2.presentation.productos.listar

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.Toast
import cl.hardroid.supermerk2.R
import cl.hardroid.supermerk2.di.scope.ProductoScope
import cl.hardroid.supermerk2.presentation.base.viewmodel.ErrorViewModel
import cl.hardroid.supermerk2.presentation.productos.viewmodel.ProductosViewModel
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_listar_productos.*
import javax.inject.Inject

class ListarProductosActivity : AppCompatActivity(), ListarProductoContract.View, View.OnClickListener {


    @Inject
    @ProductoScope
    lateinit var navigator: ListarProductoContract.Navigator

    @Inject
    @ProductoScope
    lateinit var presenter: ListarProductoContract.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_listar_productos)
        rvProductos.layoutManager = LinearLayoutManager(this)
        btnAddProducto.setOnClickListener(this)

        presenter.bindView(this)
    }

    override fun onResume() {
        super.onResume()
        presenter.loadProducts()
    }

    override fun showProgresDialog() {

    }

    override fun hideProgresDialog() {

    }

    override fun showErrorNoInternetConnection() {
        Toast.makeText(this,"Error de conexión", Toast.LENGTH_LONG).show()
    }

    override fun showError(error: ErrorViewModel) {
        Toast.makeText(this,"Error de conexión ${error.title} ${error.message}", Toast.LENGTH_LONG).show()
    }

    override fun listProducts(products: List<ProductosViewModel>) {
        rvProductos.visibility = View.VISIBLE
        txtEmptyListText.visibility = View.GONE
        rvProductos.adapter = ListarProductosAdapter(products, this)

    }

    override fun viewDetailsProduct(producto: ProductosViewModel) {
        presenter.viewDetails(producto)
    }

    override fun showEmptyList() {
        rvProductos!!.visibility = View.GONE
        txtEmptyListText!!.visibility = View.VISIBLE
    }

    override fun onClick(v: View?) {
        presenter.agregarProductoClick()
    }
}
