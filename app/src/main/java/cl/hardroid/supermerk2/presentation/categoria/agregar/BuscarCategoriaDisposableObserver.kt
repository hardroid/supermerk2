package cl.hardroid.supermerk2.presentation.categoria.agregar

import cl.hardroid.supermerk2.domain.entity.CategoriaEntity
import io.reactivex.observers.DisposableObserver

class BuscarCategoriaDisposableObserver(private val onBuscarCategoriaCallback: OnBuscarCategoriaCallback) : DisposableObserver<List<CategoriaEntity>>() {
    override fun onComplete() {

    }

    override fun onNext(t: List<CategoriaEntity>) {
        onBuscarCategoriaCallback.onBuscarCategoriasComplete(t)
    }

    override fun onError(e: Throwable) {

    }
}