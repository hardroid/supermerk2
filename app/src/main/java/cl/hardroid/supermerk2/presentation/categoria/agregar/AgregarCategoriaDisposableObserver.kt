package cl.hardroid.supermerk2.presentation.categoria.agregar

import io.reactivex.observers.DisposableObserver

class AgregarCategoriaDisposableObserver(private val onAgregarCategoriaCallback: OnAgregarCategoriaCallback) : DisposableObserver<Boolean>() {
    override fun onComplete() {

    }

    override fun onNext(t: Boolean) {
        onAgregarCategoriaCallback.onAgregarCategoriaComplete(t)
    }

    override fun onError(e: Throwable) {

    }
}