package cl.hardroid.supermerk2.presentation.base

interface MVPContract {
    interface BasePresenter<T : BaseView> {
        fun bindView(view: T)
        fun unbindView()
    }

    interface BaseView

    interface Navigator

}