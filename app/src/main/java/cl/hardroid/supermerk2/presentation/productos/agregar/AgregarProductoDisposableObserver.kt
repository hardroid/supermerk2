package cl.hardroid.supermerk2.presentation.productos.agregar

import io.reactivex.observers.DisposableObserver


class AgregarProductoDisposableObserver(private val onAgregarProductoCallback: OnAgregarProductoCallback) : DisposableObserver<Boolean>() {

    override fun onComplete() {

    }

    override fun onNext(t: Boolean) {
        onAgregarProductoCallback.onAgregarProductoComplete(t)
    }

    override fun onError(e: Throwable) {
        onAgregarProductoCallback.onAgregarProductoError(e)
    }
}
