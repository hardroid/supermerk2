package cl.hardroid.supermerk2.presentation.categoria.agregar

interface OnAgregarCategoriaCallback {
    fun onAgregarCategoriaComplete(t: Boolean)
}