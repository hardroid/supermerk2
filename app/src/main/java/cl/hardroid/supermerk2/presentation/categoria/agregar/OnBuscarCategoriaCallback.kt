package cl.hardroid.supermerk2.presentation.categoria.agregar

import cl.hardroid.supermerk2.domain.entity.CategoriaEntity

interface OnBuscarCategoriaCallback {
    fun onBuscarCategoriasComplete(list: List<CategoriaEntity>)
}
