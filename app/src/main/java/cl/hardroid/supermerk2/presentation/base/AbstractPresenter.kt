package cl.hardroid.supermerk2.presentation.base


abstract class AbstractPresenter<T : MVPContract.BaseView> : MVPContract.BasePresenter<T> {
    protected var view: T? = null
        private set

    override fun bindView(view: T) {
        this.view = view
    }

    override fun unbindView() {
        this.view = null
    }

}