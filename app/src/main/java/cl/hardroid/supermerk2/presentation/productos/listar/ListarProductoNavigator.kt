package cl.hardroid.supermerk2.presentation.productos.listar

import android.content.Intent
import android.widget.Toast
import cl.hardroid.supermerk2.presentation.productos.agregar.AgregarProductoActivity
import cl.hardroid.supermerk2.presentation.productos.viewmodel.ProductosViewModel
import javax.inject.Inject


class ListarProductoNavigator
@Inject constructor() : ListarProductoContract.Navigator {

    @Inject
    lateinit var activity: ListarProductosActivity

    override fun goToDetails(producto: ProductosViewModel) {
        Toast.makeText(activity, "Detalle", Toast.LENGTH_LONG).show()
    }

    override fun goToNewProduct() {
        activity.startActivity(Intent(activity, AgregarProductoActivity::class.java))
    }
}