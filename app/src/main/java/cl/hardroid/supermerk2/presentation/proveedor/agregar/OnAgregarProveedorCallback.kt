package cl.hardroid.supermerk2.presentation.proveedor.agregar

interface OnAgregarProveedorCallback {
    fun onAgregarProveedorComplete(t: Boolean)
}