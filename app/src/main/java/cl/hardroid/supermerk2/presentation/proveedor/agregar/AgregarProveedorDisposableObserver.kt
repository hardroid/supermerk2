package cl.hardroid.supermerk2.presentation.proveedor.agregar

import io.reactivex.observers.DisposableObserver

class AgregarProveedorDisposableObserver(private val onAgregarProveedorCallback: OnAgregarProveedorCallback) : DisposableObserver<Boolean>() {
    override fun onComplete() {

    }

    override fun onNext(t: Boolean) {
        onAgregarProveedorCallback.onAgregarProveedorComplete(t)
    }

    override fun onError(e: Throwable) {

    }
}