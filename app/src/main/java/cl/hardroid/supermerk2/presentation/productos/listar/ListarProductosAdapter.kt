package cl.hardroid.supermerk2.presentation.productos.listar

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import cl.hardroid.supermerk2.R
import cl.hardroid.supermerk2.presentation.productos.viewmodel.ProductosViewModel
import kotlinx.android.synthetic.main.producto_item.view.*

class ListarProductosAdapter(val items: List<ProductosViewModel>, val context: Context) : RecyclerView.Adapter<ListarProductosAdapter.ViewHolder>() {


    override fun getItemCount(): Int {
        return items.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.producto_item, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val productosValue = items.get(position)
        holder.bindData(productosValue)

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tvTitle = view.tvTitle
        val tvFecha = view.tvFecha
        val tvPrecio = view.tvPrice
        val tvProveedor = view.tvProveedor

        fun bindData(value : ProductosViewModel){
            tvTitle?.text = "Nombre: ${value.name}"
            tvFecha?.text = value.fecha
            tvPrecio?.text = "$${value.price}"
            tvProveedor?.text = "Proveedor: ${value.proveedor}"
        }
    }
}


