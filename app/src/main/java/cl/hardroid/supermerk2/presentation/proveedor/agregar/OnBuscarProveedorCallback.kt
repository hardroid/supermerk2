package cl.hardroid.supermerk2.presentation.proveedor.agregar

import cl.hardroid.supermerk2.domain.entity.ProveedorEntity

interface OnBuscarProveedorCallback {
    fun onBuscarProveedorComplete(list: List<ProveedorEntity>)
}
