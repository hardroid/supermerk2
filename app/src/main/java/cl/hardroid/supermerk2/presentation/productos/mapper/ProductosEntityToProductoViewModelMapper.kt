package cl.hardroid.supermerk2.presentation.productos.mapper

import cl.hardroid.supermerk2.domain.base.Mapper
import cl.hardroid.supermerk2.domain.entity.ProductoEntity
import cl.hardroid.supermerk2.presentation.productos.viewmodel.ProductosViewModel
import dagger.Reusable
import javax.inject.Inject

@Reusable
class ProductosEntityToProductoViewModelMapper
@Inject constructor() : Mapper<ProductoEntity, ProductosViewModel>() {

    override fun map(input: ProductoEntity): ProductosViewModel {
        return ProductosViewModel(
                input.id,
                input.nombre,
                input.precio.toString(),
                input.fecha,
                input.categoria,
                input.proveedor)
    }
}