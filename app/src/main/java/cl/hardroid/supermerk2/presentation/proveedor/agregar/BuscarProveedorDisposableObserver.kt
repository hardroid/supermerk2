package cl.hardroid.supermerk2.presentation.proveedor.agregar

import cl.hardroid.supermerk2.domain.entity.ProveedorEntity
import io.reactivex.observers.DisposableObserver

class BuscarProveedorDisposableObserver(private val onBuscarProveedorCallback: OnBuscarProveedorCallback) : DisposableObserver<List<ProveedorEntity>>() {
    override fun onComplete() {

    }

    override fun onNext(t: List<ProveedorEntity>) {
        onBuscarProveedorCallback.onBuscarProveedorComplete(t)
    }

    override fun onError(e: Throwable) {

    }
}