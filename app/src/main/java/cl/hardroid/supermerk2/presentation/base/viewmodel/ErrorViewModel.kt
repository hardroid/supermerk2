package cl.hardroid.supermerk2.presentation.base.viewmodel

data class ErrorViewModel(val title: String,
                          val message: String)