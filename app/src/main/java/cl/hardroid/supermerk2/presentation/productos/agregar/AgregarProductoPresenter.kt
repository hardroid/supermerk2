package cl.hardroid.supermerk2.presentation.productos.agregar

import cl.hardroid.supermerk2.domain.entity.CategoriaEntity
import cl.hardroid.supermerk2.domain.entity.ProveedorEntity
import cl.hardroid.supermerk2.domain.usecase.*
import cl.hardroid.supermerk2.presentation.base.AbstractPresenter
import cl.hardroid.supermerk2.presentation.base.viewmodel.ErrorViewModel
import cl.hardroid.supermerk2.presentation.categoria.agregar.AgregarCategoriaDisposableObserver
import cl.hardroid.supermerk2.presentation.categoria.agregar.BuscarCategoriaDisposableObserver
import cl.hardroid.supermerk2.presentation.categoria.agregar.OnAgregarCategoriaCallback
import cl.hardroid.supermerk2.presentation.categoria.agregar.OnBuscarCategoriaCallback
import cl.hardroid.supermerk2.presentation.categoria.mapper.CategoriaEntityToStringMapper
import cl.hardroid.supermerk2.presentation.productos.viewmodel.ProductosViewModel
import cl.hardroid.supermerk2.presentation.proveedor.agregar.AgregarProveedorDisposableObserver
import cl.hardroid.supermerk2.presentation.proveedor.agregar.BuscarProveedorDisposableObserver
import cl.hardroid.supermerk2.presentation.proveedor.agregar.OnAgregarProveedorCallback
import cl.hardroid.supermerk2.presentation.proveedor.agregar.OnBuscarProveedorCallback
import cl.hardroid.supermerk2.presentation.proveedor.mapper.ProveedorEntityToStringMapper
import javax.inject.Inject

class AgregarProductoPresenter
@Inject constructor() : AbstractPresenter<AgregarProductoContract.View>(),
        AgregarProductoContract.Presenter,
        OnAgregarProductoCallback,
        OnBuscarCategoriaCallback,
        OnAgregarCategoriaCallback,
        OnBuscarProveedorCallback,
        OnAgregarProveedorCallback{

    @Inject
    lateinit var navigator: AgregarProductoContract.Navigator

    @Inject
    lateinit var addProductUseCase: AddProductUseCase

    @Inject
    lateinit var addCategoriaUseCase: AddCategoriaUseCase

    @Inject
    lateinit var addProveedorUseCase: AddProveedorUseCase

    @Inject
    lateinit var getCategoriaUseCase: GetCategoriaUseCase

    @Inject
    lateinit var getProveedorUseCase: GetProveedoresUseCase

    @Inject
    lateinit var mapperCategoria: CategoriaEntityToStringMapper

    @Inject
    lateinit var mapperProveedor: ProveedorEntityToStringMapper

    override fun agregarProducto(producto: ProductosViewModel) {
        if(validaDatos(producto)) {
            view?.showProgresDialog()

            addProductUseCase.execute(
                    AgregarProductoDisposableObserver(this),
                    AddProductUseCase.Params(producto.name, producto.categoria, producto.proveedor, producto.fecha, producto.price.toInt())
            )

            addProveedorUseCase.execute(AgregarProveedorDisposableObserver(this),
                    AddProveedorUseCase.Params(producto.proveedor))

            addCategoriaUseCase.execute(AgregarCategoriaDisposableObserver(this),
                    AddCategoriaUseCase.Params(producto.categoria))
        }else{
            view?.showMessageIncompleteForm()
        }
    }

    override fun validaDatos(producto: ProductosViewModel): Boolean {
        return !producto.categoria.isEmpty() &&
                !producto.fecha.isEmpty() &&
                !producto.name.isEmpty() &&
                !producto.price.isEmpty() &&
                !producto.proveedor.isEmpty()
    }

    override fun buscaCategorias(filter: String) {
        getCategoriaUseCase.execute(BuscarCategoriaDisposableObserver(this),filter)
    }

    override fun buscaProveedores(filter: String) {
        getProveedorUseCase.execute(BuscarProveedorDisposableObserver(this), filter)
    }

    override fun onAgregarProductoComplete(t: Boolean) {
        if (t) {
            view?.cleanForm()
            view?.hideProgresDialog()
            view?.showMessageRegisterOk()
        } else {
            view?.hideProgresDialog()
            view?.showError(ErrorViewModel("Error al guardar", "No se pudo guardar el producto"))
        }
    }

    override fun onAgregarProductoError(e: Throwable) {
        view?.hideProgresDialog()
        view?.showError(ErrorViewModel("Error al guardar", "${e.message}"))
    }

    override fun onBuscarCategoriasComplete(list: List<CategoriaEntity>) {
        if(!list.isEmpty()){
            view?.obtieneCategorias(mapperCategoria.mapAll(list))
        }
    }

    override fun onAgregarCategoriaComplete(t: Boolean) {
        if(t){
            //TODO cargar nuevamente las categorías
        }
    }

    override fun onBuscarProveedorComplete(list: List<ProveedorEntity>) {
        if(!list.isEmpty()){
            view?.obtieneProveedores(mapperProveedor.mapAll(list))
        }
    }

    override fun onAgregarProveedorComplete(t: Boolean) {
        if(t){
            //TODO cargar nuevamente los proveedores
        }
    }
}