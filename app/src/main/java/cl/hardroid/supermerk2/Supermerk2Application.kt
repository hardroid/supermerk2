package cl.hardroid.supermerk2

import android.app.Activity
import android.arch.persistence.room.Room
import cl.hardroid.supermerk2.data.datasource.local.Supermerk2DataBase
import cl.hardroid.supermerk2.di.component.DaggerSupermerk2Component
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import dagger.android.DispatchingAndroidInjector
import javax.inject.Inject

class Supermerk2Application : DaggerApplication() {

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Activity>

    companion object {
        var database: Supermerk2DataBase? = null
    }

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerSupermerk2Component.builder().create(this)
    }

    override fun onCreate() {
        super.onCreate()
        Supermerk2Application.database =
                Room.databaseBuilder(
                        this,
                        Supermerk2DataBase::class.java,
                        "supermerk2-db").build()
    }
}