package cl.hardroid.supermerk2.domain.base.executor

import io.reactivex.Scheduler
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class JobExecutor
@Inject constructor() : ThreadExecutor {

    override val scheduler: Scheduler
        get() = Schedulers.io()
}