package cl.hardroid.supermerk2.domain.repository

import cl.hardroid.supermerk2.domain.entity.ProveedorEntity
import cl.hardroid.supermerk2.domain.usecase.AddProveedorUseCase
import io.reactivex.Observable

interface ProveedorRepository {
    fun obtainProveedor(filter: String) : Observable<List<ProveedorEntity>>
    fun addProveedor(params: AddProveedorUseCase.Params) : Observable<Boolean>
}