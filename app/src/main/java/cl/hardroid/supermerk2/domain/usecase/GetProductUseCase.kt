package cl.hardroid.supermerk2.domain.usecase

import cl.hardroid.supermerk2.domain.base.executor.PostExecutionThread
import cl.hardroid.supermerk2.domain.base.RxUseCase
import cl.hardroid.supermerk2.domain.base.executor.ThreadExecutor
import cl.hardroid.supermerk2.domain.entity.ProductoEntity
import cl.hardroid.supermerk2.domain.repository.ProductoRepository
import io.reactivex.Observable
import javax.inject.Inject

class GetProductUseCase : RxUseCase<List<ProductoEntity>, Void> {

    @Inject
    lateinit var repository: ProductoRepository

    @Inject
    constructor(threadExecutor: ThreadExecutor, postExecutionThread: PostExecutionThread) : super(threadExecutor, postExecutionThread)

    override fun buildObservable(params: Void?): Observable<List<ProductoEntity>> {
        return repository.obtainProducts()
    }

}