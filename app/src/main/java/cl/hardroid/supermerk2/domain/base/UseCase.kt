package cl.hardroid.supermerk2.domain.base

interface UseCase<I, Params> {
    fun execute(callback: I, params: Params?)
    fun stop()
}
