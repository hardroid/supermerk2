package cl.hardroid.supermerk2.domain.base.executor

import io.reactivex.Scheduler

interface ThreadExecutor {
    val scheduler: Scheduler
}