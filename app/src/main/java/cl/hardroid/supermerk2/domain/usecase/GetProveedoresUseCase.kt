package cl.hardroid.supermerk2.domain.usecase

import cl.hardroid.supermerk2.domain.base.executor.PostExecutionThread
import cl.hardroid.supermerk2.domain.base.RxUseCase
import cl.hardroid.supermerk2.domain.base.executor.ThreadExecutor
import cl.hardroid.supermerk2.domain.entity.CategoriaEntity
import cl.hardroid.supermerk2.domain.entity.ProveedorEntity
import cl.hardroid.supermerk2.domain.repository.CategoriaRepository
import cl.hardroid.supermerk2.domain.repository.ProveedorRepository
import io.reactivex.Observable
import io.reactivex.observers.DisposableObserver
import javax.inject.Inject

class GetProveedoresUseCase : RxUseCase<List<ProveedorEntity>, String> {

    @Inject
    lateinit var repository: ProveedorRepository

    @Inject
    constructor(threadExecutor: ThreadExecutor, postExecutionThread: PostExecutionThread) : super(threadExecutor, postExecutionThread)

    override fun buildObservable(params: String?): Observable<List<ProveedorEntity>> {
        return repository.obtainProveedor(params!!)
    }

}