package cl.hardroid.supermerk2.domain.entity

data class CategoriaEntity(
        val id: Long,
        val nombre: String)