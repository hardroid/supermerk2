package cl.hardroid.supermerk2.domain.entity

data class ProveedorEntity(
        val id: Long,
        val nombre: String)