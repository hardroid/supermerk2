package cl.hardroid.supermerk2.domain.repository

import cl.hardroid.supermerk2.domain.entity.CategoriaEntity
import cl.hardroid.supermerk2.domain.usecase.AddCategoriaUseCase
import io.reactivex.Observable

interface CategoriaRepository {
    fun obtainCategories(filter: String) : Observable<List<CategoriaEntity>>
    fun addCategories(params: AddCategoriaUseCase.Params) : Observable<Boolean>
}