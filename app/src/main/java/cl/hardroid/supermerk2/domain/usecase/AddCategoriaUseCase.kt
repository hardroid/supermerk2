package cl.hardroid.supermerk2.domain.usecase

import cl.hardroid.supermerk2.domain.base.executor.PostExecutionThread
import cl.hardroid.supermerk2.domain.base.RxUseCase
import cl.hardroid.supermerk2.domain.base.executor.ThreadExecutor
import cl.hardroid.supermerk2.domain.repository.CategoriaRepository
import io.reactivex.Observable
import javax.inject.Inject

class AddCategoriaUseCase
@Inject constructor(threadExecutor: ThreadExecutor, postExecutionThread: PostExecutionThread) : RxUseCase<Boolean, AddCategoriaUseCase.Params>(threadExecutor, postExecutionThread) {

    @Inject
    lateinit var repository: CategoriaRepository

    override fun buildObservable(params: Params?): Observable<Boolean> {
        return repository.addCategories(params!!)
    }

    data class Params(
            val nombre: String
    )

}