package cl.hardroid.supermerk2.domain.repository

import cl.hardroid.supermerk2.domain.entity.ProductoEntity
import cl.hardroid.supermerk2.domain.usecase.AddProductUseCase
import io.reactivex.Observable

interface ProductoRepository {
    fun obtainProducts() : Observable<List<ProductoEntity>>
    fun obtainProducts(filter : String) : Observable<List<ProductoEntity>>
    fun addProduct(params: AddProductUseCase.Params) : Observable<Boolean>
}