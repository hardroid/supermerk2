package cl.hardroid.supermerk2.domain.base

import cl.hardroid.supermerk2.domain.base.executor.PostExecutionThread
import cl.hardroid.supermerk2.domain.base.executor.ThreadExecutor
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.observers.DisposableObserver


abstract class RxUseCase<T, Params>(private val threadExecutor: ThreadExecutor, private val postExecutionThread: PostExecutionThread) : UseCase<DisposableObserver<T>, Params> {
    private val disposable: CompositeDisposable

    init {
        this.disposable = CompositeDisposable()
    }

    abstract fun buildObservable(params: Params?): Observable<T>

    override fun execute(callback: DisposableObserver<T>, params: Params?) {
        val observable = buildObservable(params)
                .subscribeOn(threadExecutor.scheduler)
                .observeOn(postExecutionThread.scheduler)
        addDisposable(observable.subscribeWith(callback))
    }

    fun execute(observer: DisposableObserver<T>) {
        execute(observer, null)
    }

    private fun addDisposable(observer: Disposable) {
        disposable.add(observer)
    }

    private fun dispose() {
        if (!disposable.isDisposed) {
            disposable.dispose()
        }
    }

    override fun stop() {
        dispose()
    }
}