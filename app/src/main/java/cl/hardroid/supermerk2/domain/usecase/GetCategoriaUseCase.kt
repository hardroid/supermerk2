package cl.hardroid.supermerk2.domain.usecase

import cl.hardroid.supermerk2.domain.base.executor.PostExecutionThread
import cl.hardroid.supermerk2.domain.base.RxUseCase
import cl.hardroid.supermerk2.domain.base.executor.ThreadExecutor
import cl.hardroid.supermerk2.domain.entity.CategoriaEntity
import cl.hardroid.supermerk2.domain.repository.CategoriaRepository
import io.reactivex.Observable
import io.reactivex.observers.DisposableObserver
import javax.inject.Inject

class GetCategoriaUseCase : RxUseCase<List<CategoriaEntity>, String> {

    @Inject
    lateinit var repository: CategoriaRepository

    @Inject
    constructor(threadExecutor: ThreadExecutor, postExecutionThread: PostExecutionThread) : super(threadExecutor, postExecutionThread)

    override fun buildObservable(params: String?): Observable<List<CategoriaEntity>> {
        return repository.obtainCategories(params!!)
    }

}