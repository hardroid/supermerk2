package cl.hardroid.supermerk2.domain.usecase

import cl.hardroid.supermerk2.domain.base.executor.PostExecutionThread
import cl.hardroid.supermerk2.domain.base.RxUseCase
import cl.hardroid.supermerk2.domain.base.executor.ThreadExecutor
import cl.hardroid.supermerk2.domain.entity.ProductoEntity
import cl.hardroid.supermerk2.domain.repository.ProductoRepository
import cl.hardroid.supermerk2.domain.repository.ProveedorRepository
import io.reactivex.Observable
import javax.inject.Inject

class AddProveedorUseCase
@Inject constructor(threadExecutor: ThreadExecutor, postExecutionThread: PostExecutionThread) : RxUseCase<Boolean, AddProveedorUseCase.Params>(threadExecutor, postExecutionThread) {

    @Inject
    lateinit var repository: ProveedorRepository

    override fun buildObservable(params: Params?): Observable<Boolean> {
        return repository.addProveedor(params!!)
    }

    data class Params(
            val nombre: String
    )

}