package cl.hardroid.supermerk2.domain.usecase

import cl.hardroid.supermerk2.domain.base.executor.PostExecutionThread
import cl.hardroid.supermerk2.domain.base.RxUseCase
import cl.hardroid.supermerk2.domain.base.executor.ThreadExecutor
import cl.hardroid.supermerk2.domain.entity.ProductoEntity
import cl.hardroid.supermerk2.domain.repository.ProductoRepository
import io.reactivex.Observable
import javax.inject.Inject

class AddProductUseCase
@Inject constructor(threadExecutor: ThreadExecutor, postExecutionThread: PostExecutionThread) : RxUseCase<Boolean, AddProductUseCase.Params>(threadExecutor, postExecutionThread) {

    @Inject
    lateinit var repository: ProductoRepository

    override fun buildObservable(params: Params?): Observable<Boolean> {
        return repository.addProduct(params!!)
    }

    data class Params(
            val nombre: String,
            val categoria: String,
            val proveedor: String,
            val fecha: String,
            val precio: Int
    )

}