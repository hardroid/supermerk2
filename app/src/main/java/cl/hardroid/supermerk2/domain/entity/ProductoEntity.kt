package cl.hardroid.supermerk2.domain.entity

data class ProductoEntity(
        val id: Long,
        val nombre: String,
        val categoria: String,
        val fecha: String,
        val precio: Int,
        val proveedor: String)