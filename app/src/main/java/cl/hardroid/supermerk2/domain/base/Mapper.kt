package cl.hardroid.supermerk2.domain.base

import java.util.ArrayList

abstract class Mapper<I, O> {
    abstract fun map(input: I): O

    fun mapAll(inputs: List<I>): List<O> {
        val list = ArrayList<O>()
        for (input in inputs) {
            list.add(map(input))
        }
        return list
    }
}