package cl.hardroid.supermerk2.data.datasource.local.model

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity
data class ProductoModel(
        @PrimaryKey(autoGenerate = true) val id : Long,
        val nombre : String,
        val categoria : String,
        val fecha: String,
        val precio : Int,
        val proveedor : String)