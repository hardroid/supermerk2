package cl.hardroid.supermerk2.data.datasource.local.model

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity
data class ProveedorModel(
        @PrimaryKey(autoGenerate = true) val id : Long,
        val nombre : String
)