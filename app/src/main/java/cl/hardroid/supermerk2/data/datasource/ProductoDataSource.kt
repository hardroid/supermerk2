package cl.hardroid.supermerk2.data.datasource

import cl.hardroid.supermerk2.domain.entity.ProductoEntity
import cl.hardroid.supermerk2.domain.usecase.AddProductUseCase
import io.reactivex.Observable

interface ProductoDataSource {

    fun obtenerProductos() : Observable<List<ProductoEntity>>
    fun obtenerProductos(filter: String) : Observable<List<ProductoEntity>>
    fun agregarProducto(params: AddProductUseCase.Params) : Observable<Boolean>
}