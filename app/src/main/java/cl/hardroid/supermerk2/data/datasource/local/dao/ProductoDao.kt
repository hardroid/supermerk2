package cl.hardroid.supermerk2.data.datasource.local.dao

import android.arch.persistence.room.*
import cl.hardroid.supermerk2.data.datasource.local.model.ProductoModel
import io.reactivex.Flowable

@Dao
interface ProductoDao {
    @Query("SELECT * FROM ProductoModel")
    fun getAllProductos() : Flowable<List<ProductoModel>>

    @Query("SELECT * FROM ProductoModel WHERE nombre LIKE :filter")
    fun getFilterProductos(filter : String) : Flowable<List<ProductoModel>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertProducto(productoModel: ProductoModel) : Long

    @Update
    fun updateProducto(list: Array<ProductoModel>)

    @Delete
    fun deleteProducto(list: Array<ProductoModel>)
}