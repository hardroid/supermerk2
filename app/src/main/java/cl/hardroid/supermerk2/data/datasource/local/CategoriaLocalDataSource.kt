package cl.hardroid.supermerk2.data.datasource.local

import cl.hardroid.supermerk2.data.datasource.CategoriaDataSource
import cl.hardroid.supermerk2.data.datasource.local.model.CategoriaModel
import cl.hardroid.supermerk2.data.datasource.mapper.CategoriaModelToCategoriaEntity
import cl.hardroid.supermerk2.domain.entity.CategoriaEntity
import cl.hardroid.supermerk2.domain.usecase.AddCategoriaUseCase
import io.reactivex.Observable
import javax.inject.Inject

class CategoriaLocalDataSource
@Inject constructor() : CategoriaDataSource {

    @Inject
    lateinit var db: Supermerk2DataBase

    @Inject
    lateinit var mapper: CategoriaModelToCategoriaEntity


    override fun obtenerCategorias(filter: String): Observable<List<CategoriaEntity>> =
            if (filter.isEmpty()) {
                db.categoriaDao()
                        .getAllCategorias()
                        .toObservable()
                        .map { response ->
                            mapper.mapAll(response)
                        }
            } else {
                db.categoriaDao()
                        .getAllCategorias(filter)
                        .toObservable()
                        .map { response ->
                            mapper.mapAll(response)
                        }

            }

    override fun agregarCategoria(params: AddCategoriaUseCase.Params): Observable<Boolean> {
        return Observable.create<Boolean> { subscriber ->
            val respuesta = db.categoriaDao()
                    .insertCategoria(
                            CategoriaModel(
                                    0,
                                    params.nombre)) > 0
            subscriber.onNext(respuesta)
            subscriber.onComplete()
        }
    }
}