package cl.hardroid.supermerk2.data.repository

import cl.hardroid.supermerk2.data.datasource.CategoriaDataSource
import cl.hardroid.supermerk2.data.datasource.ProveedorDataSource
import cl.hardroid.supermerk2.domain.entity.CategoriaEntity
import cl.hardroid.supermerk2.domain.entity.ProveedorEntity
import cl.hardroid.supermerk2.domain.repository.CategoriaRepository
import cl.hardroid.supermerk2.domain.repository.ProveedorRepository
import cl.hardroid.supermerk2.domain.usecase.AddCategoriaUseCase
import cl.hardroid.supermerk2.domain.usecase.AddProveedorUseCase
import io.reactivex.Observable
import javax.inject.Inject

class ProveedorDataRepository
@Inject constructor() : ProveedorRepository {

    @Inject
    lateinit var dataSource: ProveedorDataSource

    override fun obtainProveedor(filter: String): Observable<List<ProveedorEntity>> {
        return dataSource.obtenerProveedores(filter)
    }

    override fun addProveedor(params: AddProveedorUseCase.Params): Observable<Boolean> {
        return dataSource.agregarProveedor(params)
    }

}