package cl.hardroid.supermerk2.data.repository

import cl.hardroid.supermerk2.data.datasource.CategoriaDataSource
import cl.hardroid.supermerk2.domain.entity.CategoriaEntity
import cl.hardroid.supermerk2.domain.repository.CategoriaRepository
import cl.hardroid.supermerk2.domain.usecase.AddCategoriaUseCase
import io.reactivex.Observable
import javax.inject.Inject

class CategoriaDataRepository
@Inject constructor() : CategoriaRepository {

    @Inject
    lateinit var dataSource: CategoriaDataSource


    override fun obtainCategories(filter: String): Observable<List<CategoriaEntity>> {
        return dataSource.obtenerCategorias(filter)
    }

    override fun addCategories(params: AddCategoriaUseCase.Params): Observable<Boolean> {
        return dataSource.agregarCategoria(params)
    }
}