package cl.hardroid.supermerk2.data.datasource.local.dao

import android.arch.persistence.room.*
import cl.hardroid.supermerk2.data.datasource.local.model.ProveedorModel
import io.reactivex.Flowable

@Dao
interface ProveedorDao {
    @Query("SELECT * FROM ProveedorModel")
    fun getAllProveedores() : Flowable<List<ProveedorModel>>

    @Query("SELECT * FROM ProveedorModel WHERE nombre LIKE :filter")
    fun getAllProveedores(filter: String) : Flowable<List<ProveedorModel>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertProveedor(proveedorModel: ProveedorModel) : Long

    @Update
    fun updateProveedor(list: Array<ProveedorModel>)

    @Delete
    fun deleteProveedor(list: Array<ProveedorModel>)
}