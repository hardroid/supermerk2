package cl.hardroid.supermerk2.data.datasource.local

import cl.hardroid.supermerk2.data.datasource.CategoriaDataSource
import cl.hardroid.supermerk2.data.datasource.ProveedorDataSource
import cl.hardroid.supermerk2.data.datasource.local.model.CategoriaModel
import cl.hardroid.supermerk2.data.datasource.local.model.ProveedorModel
import cl.hardroid.supermerk2.data.datasource.mapper.CategoriaModelToCategoriaEntity
import cl.hardroid.supermerk2.data.datasource.mapper.ProveedorModelToProveedorEntity
import cl.hardroid.supermerk2.domain.entity.CategoriaEntity
import cl.hardroid.supermerk2.domain.entity.ProveedorEntity
import cl.hardroid.supermerk2.domain.usecase.AddCategoriaUseCase
import cl.hardroid.supermerk2.domain.usecase.AddProveedorUseCase
import io.reactivex.Observable
import javax.inject.Inject

class ProveedorLocalDataSource
@Inject constructor() : ProveedorDataSource {

    @Inject
    lateinit var db: Supermerk2DataBase

    @Inject
    lateinit var mapper: ProveedorModelToProveedorEntity

    override fun obtenerProveedores(filter: String): Observable<List<ProveedorEntity>> =
            if (filter.isEmpty()) {
                db.proveedorDao()
                        .getAllProveedores()
                        .toObservable()
                        .map { response ->
                            mapper.mapAll(response)
                        }
            } else {
                db.proveedorDao()
                        .getAllProveedores(filter)
                        .toObservable()
                        .map { response ->
                            mapper.mapAll(response)
                        }

            }


    override fun agregarProveedor(params: AddProveedorUseCase.Params): Observable<Boolean> =
            Observable.create<Boolean> { subscriber ->
                val respuesta = db.proveedorDao()
                        .insertProveedor(
                                ProveedorModel(
                                        0,
                                        params.nombre)) > 0
                subscriber.onNext(respuesta)
                subscriber.onComplete()

            }
}