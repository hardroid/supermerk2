package cl.hardroid.supermerk2.data.datasource.local

import cl.hardroid.supermerk2.Supermerk2Application
import cl.hardroid.supermerk2.data.datasource.ProductoDataSource
import cl.hardroid.supermerk2.data.datasource.local.model.ProductoModel
import cl.hardroid.supermerk2.data.datasource.mapper.ProductoModelToProductoEntity
import cl.hardroid.supermerk2.domain.entity.ProductoEntity
import cl.hardroid.supermerk2.domain.usecase.AddProductUseCase
import io.reactivex.Observable
import io.reactivex.ObservableOnSubscribe
import io.reactivex.Scheduler
import org.reactivestreams.Subscriber
import javax.inject.Inject

class ProductoLocalDataSource
@Inject constructor() : ProductoDataSource {

    @Inject
    lateinit var db: Supermerk2DataBase

    @Inject
    lateinit var mapper: ProductoModelToProductoEntity

    override fun obtenerProductos(): Observable<List<ProductoEntity>> {
        return db.productoDao()
                .getAllProductos()
                .toObservable()
                .map { response ->
                    mapper.mapAll(response)
                }
    }

    override fun obtenerProductos(filter: String): Observable<List<ProductoEntity>> {
        return db.productoDao()
                .getFilterProductos(filter)
                .toObservable()
                .map { response ->
                    mapper.mapAll(response)
                }
    }

    override fun agregarProducto(params: AddProductUseCase.Params): Observable<Boolean> {
        return Observable.create<Boolean> { subscriber ->

            val respuesta = db.productoDao()
                    .insertProducto(
                            ProductoModel(
                                    0,
                                    params.nombre,
                                    params.categoria,
                                    params.fecha,
                                    params.precio,
                                    params.proveedor)) > 0
            subscriber.onNext(respuesta)
            subscriber.onComplete()
        }
    }
}