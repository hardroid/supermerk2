package cl.hardroid.supermerk2.data.datasource.mapper

import cl.hardroid.supermerk2.data.datasource.local.model.CategoriaModel
import cl.hardroid.supermerk2.domain.base.Mapper
import cl.hardroid.supermerk2.domain.entity.CategoriaEntity
import javax.inject.Inject

class CategoriaModelToCategoriaEntity
@Inject constructor() : Mapper<CategoriaModel, CategoriaEntity>() {

    override fun map(input: CategoriaModel): CategoriaEntity {
        return CategoriaEntity(input.id, input.nombre)
    }
}