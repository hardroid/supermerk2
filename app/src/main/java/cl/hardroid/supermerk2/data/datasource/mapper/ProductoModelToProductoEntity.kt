package cl.hardroid.supermerk2.data.datasource.mapper

import cl.hardroid.supermerk2.data.datasource.local.model.ProductoModel
import cl.hardroid.supermerk2.domain.base.Mapper
import cl.hardroid.supermerk2.domain.entity.ProductoEntity
import javax.inject.Inject

class ProductoModelToProductoEntity
@Inject constructor() : Mapper<ProductoModel, ProductoEntity>() {

    override fun map(input: ProductoModel): ProductoEntity {
        return ProductoEntity(input.id, input.nombre, input.categoria, input.fecha, input.precio, input.proveedor)
    }
}