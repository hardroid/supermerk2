package cl.hardroid.supermerk2.data.datasource

import cl.hardroid.supermerk2.domain.entity.ProveedorEntity
import cl.hardroid.supermerk2.domain.usecase.AddProveedorUseCase
import io.reactivex.Observable

interface ProveedorDataSource {

    fun obtenerProveedores(filter: String): Observable<List<ProveedorEntity>>

    fun agregarProveedor(params: AddProveedorUseCase.Params): Observable<Boolean>
}