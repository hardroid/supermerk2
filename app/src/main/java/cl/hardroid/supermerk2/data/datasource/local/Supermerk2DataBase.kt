package cl.hardroid.supermerk2.data.datasource.local

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import cl.hardroid.supermerk2.data.datasource.local.dao.CategoriaDao
import cl.hardroid.supermerk2.data.datasource.local.dao.ProductoDao
import cl.hardroid.supermerk2.data.datasource.local.dao.ProveedorDao
import cl.hardroid.supermerk2.data.datasource.local.model.CategoriaModel
import cl.hardroid.supermerk2.data.datasource.local.model.ProductoModel
import cl.hardroid.supermerk2.data.datasource.local.model.ProveedorModel

@Database(
        entities = arrayOf(
                CategoriaModel::class,
                ProductoModel::class,
                ProveedorModel::class
        ),
        version = 4
)
abstract class Supermerk2DataBase : RoomDatabase() {
    abstract fun categoriaDao(): CategoriaDao
    abstract fun proveedorDao(): ProveedorDao
    abstract fun productoDao(): ProductoDao
}