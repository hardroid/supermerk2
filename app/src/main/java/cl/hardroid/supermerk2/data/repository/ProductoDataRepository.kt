package cl.hardroid.supermerk2.data.repository

import cl.hardroid.supermerk2.data.datasource.ProductoDataSource
import cl.hardroid.supermerk2.domain.entity.ProductoEntity
import cl.hardroid.supermerk2.domain.repository.ProductoRepository
import cl.hardroid.supermerk2.domain.usecase.AddProductUseCase
import io.reactivex.Observable
import javax.inject.Inject

class ProductoDataRepository
@Inject constructor() : ProductoRepository {

    @Inject
    lateinit var dataSource: ProductoDataSource

    override fun obtainProducts(): Observable<List<ProductoEntity>> {
        return dataSource.obtenerProductos()
    }

    override fun obtainProducts(filter: String): Observable<List<ProductoEntity>> {
        return dataSource.obtenerProductos(filter)
    }

    override fun addProduct(params: AddProductUseCase.Params): Observable<Boolean> {
        return dataSource.agregarProducto(params)
    }
}