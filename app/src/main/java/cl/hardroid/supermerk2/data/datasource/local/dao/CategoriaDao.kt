package cl.hardroid.supermerk2.data.datasource.local.dao

import android.arch.persistence.room.*
import cl.hardroid.supermerk2.data.datasource.local.model.CategoriaModel
import io.reactivex.Flowable

@Dao
interface CategoriaDao {
    @Query("SELECT * FROM CategoriaModel")
    fun getAllCategorias() : Flowable<List<CategoriaModel>>

    @Query("SELECT * FROM CategoriaModel WHERE nombre LIKE :filter")
    fun getAllCategorias(filter: String) : Flowable<List<CategoriaModel>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCategoria(categoriaModel: CategoriaModel) : Long

    @Update
    fun updateCategorias(list: Array<CategoriaModel>)

    @Delete
    fun deleteCategorias(list: Array<CategoriaModel>)
}