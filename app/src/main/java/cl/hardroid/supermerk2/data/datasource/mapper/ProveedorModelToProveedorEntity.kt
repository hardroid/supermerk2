package cl.hardroid.supermerk2.data.datasource.mapper

import cl.hardroid.supermerk2.data.datasource.local.model.CategoriaModel
import cl.hardroid.supermerk2.data.datasource.local.model.ProveedorModel
import cl.hardroid.supermerk2.domain.base.Mapper
import cl.hardroid.supermerk2.domain.entity.CategoriaEntity
import cl.hardroid.supermerk2.domain.entity.ProveedorEntity
import javax.inject.Inject

class ProveedorModelToProveedorEntity
@Inject constructor() : Mapper<ProveedorModel, ProveedorEntity>() {

    override fun map(input: ProveedorModel): ProveedorEntity {
        return ProveedorEntity(input.id, input.nombre)
    }
}