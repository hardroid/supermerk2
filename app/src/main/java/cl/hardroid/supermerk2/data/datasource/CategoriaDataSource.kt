package cl.hardroid.supermerk2.data.datasource

import cl.hardroid.supermerk2.domain.entity.CategoriaEntity
import cl.hardroid.supermerk2.domain.usecase.AddCategoriaUseCase
import io.reactivex.Observable

interface CategoriaDataSource {

    fun obtenerCategorias(filter: String): Observable<List<CategoriaEntity>>

    fun agregarCategoria(params: AddCategoriaUseCase.Params): Observable<Boolean>
}