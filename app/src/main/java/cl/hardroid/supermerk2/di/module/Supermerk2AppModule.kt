package cl.hardroid.supermerk2.di.module

import android.content.Context
import cl.hardroid.supermerk2.Supermerk2Application
import cl.hardroid.supermerk2.data.datasource.local.Supermerk2DataBase
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
abstract class Supermerk2AppModule {

    @Binds
    @Singleton
    abstract fun provideContext(application: Supermerk2Application): Context

}