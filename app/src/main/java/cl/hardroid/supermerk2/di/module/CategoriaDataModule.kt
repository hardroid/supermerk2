package cl.hardroid.supermerk2.di.module

import cl.hardroid.supermerk2.data.datasource.CategoriaDataSource
import cl.hardroid.supermerk2.data.datasource.local.CategoriaLocalDataSource
import cl.hardroid.supermerk2.data.repository.CategoriaDataRepository
import cl.hardroid.supermerk2.domain.repository.CategoriaRepository
import dagger.Module
import dagger.Provides
import dagger.Reusable

@Module
class CategoriaDataModule {

    @Reusable
    @Provides
    fun provideCategoriaRepository(repository: CategoriaDataRepository) : CategoriaRepository{
        return repository
    }

    @Reusable
    @Provides
    fun provideDataSource(dataSource: CategoriaLocalDataSource) : CategoriaDataSource{
        return dataSource
    }
}