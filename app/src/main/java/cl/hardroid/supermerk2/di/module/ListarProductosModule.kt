package cl.hardroid.supermerk2.di.module

import cl.hardroid.supermerk2.di.scope.ProductoScope
import cl.hardroid.supermerk2.presentation.productos.listar.ListarProductoContract
import cl.hardroid.supermerk2.presentation.productos.listar.ListarProductoNavigator
import cl.hardroid.supermerk2.presentation.productos.listar.ListarProductosPresenter
import dagger.Module
import dagger.Provides

@Module
class ListarProductosModule {

    @ProductoScope
    @Provides
    fun provideListProductPresenter(presenter : ListarProductosPresenter) : ListarProductoContract.Presenter{
        return presenter
    }

    @ProductoScope
    @Provides
    fun provideListProductNavigator(navigator: ListarProductoNavigator) : ListarProductoContract.Navigator{
        return navigator
    }


}