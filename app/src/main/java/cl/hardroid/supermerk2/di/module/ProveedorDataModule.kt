package cl.hardroid.supermerk2.di.module

import cl.hardroid.supermerk2.data.datasource.CategoriaDataSource
import cl.hardroid.supermerk2.data.datasource.ProveedorDataSource
import cl.hardroid.supermerk2.data.datasource.local.CategoriaLocalDataSource
import cl.hardroid.supermerk2.data.datasource.local.ProveedorLocalDataSource
import cl.hardroid.supermerk2.data.repository.CategoriaDataRepository
import cl.hardroid.supermerk2.data.repository.ProveedorDataRepository
import cl.hardroid.supermerk2.domain.repository.CategoriaRepository
import cl.hardroid.supermerk2.domain.repository.ProveedorRepository
import dagger.Module
import dagger.Provides
import dagger.Reusable

@Module
class ProveedorDataModule {

    @Reusable
    @Provides
    fun provideProveedorRepository(repository: ProveedorDataRepository) : ProveedorRepository{
        return repository
    }

    @Reusable
    @Provides
    fun provideDataSource(dataSource: ProveedorLocalDataSource) : ProveedorDataSource{
        return dataSource
    }
}