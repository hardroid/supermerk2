package cl.hardroid.supermerk2.di.module

import cl.hardroid.supermerk2.di.scope.ProductoScope
import cl.hardroid.supermerk2.presentation.productos.agregar.AgregarProductoActivity
import cl.hardroid.supermerk2.presentation.productos.listar.ListarProductosActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityModule {

    @ProductoScope
    @ContributesAndroidInjector(modules = arrayOf(ListarProductosModule::class))
    abstract fun provideListProductsActivity() : ListarProductosActivity

    @ProductoScope
    @ContributesAndroidInjector(modules = arrayOf(AgregarProductosModule::class))
    abstract fun provideAddProductActivity() : AgregarProductoActivity

}