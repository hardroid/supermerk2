package cl.hardroid.supermerk2.di.module

import cl.hardroid.supermerk2.di.scope.ProductoScope
import cl.hardroid.supermerk2.presentation.productos.agregar.AgregarProductoContract
import cl.hardroid.supermerk2.presentation.productos.agregar.AgregarProductoNavigator
import cl.hardroid.supermerk2.presentation.productos.agregar.AgregarProductoPresenter
import dagger.Module
import dagger.Provides

@Module
class AgregarProductosModule {

    @ProductoScope
    @Provides
    fun provideAgregarProductoPresenter(presenter : AgregarProductoPresenter) : AgregarProductoContract.Presenter = presenter

    @ProductoScope
    @Provides
    fun provideAgregarProductoNavigator(navigator : AgregarProductoNavigator) : AgregarProductoContract.Navigator  = navigator

}