package cl.hardroid.supermerk2.di.module

import cl.hardroid.supermerk2.domain.base.executor.JobExecutor
import cl.hardroid.supermerk2.domain.base.executor.PostExecutionThread
import cl.hardroid.supermerk2.domain.base.executor.ThreadExecutor
import cl.hardroid.supermerk2.domain.base.executor.UiThread
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ThreadModule {

    @Singleton
    @Provides
    fun provideThreadExecutor(executor: JobExecutor): ThreadExecutor {
        return executor
    }

    @Singleton
    @Provides
    fun providePostExecutionThread(thread: UiThread): PostExecutionThread {
        return thread
    }

}
