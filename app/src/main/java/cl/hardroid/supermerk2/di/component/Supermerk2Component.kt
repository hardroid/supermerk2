package cl.hardroid.supermerk2.di.component

import cl.hardroid.supermerk2.Supermerk2Application
import cl.hardroid.supermerk2.di.module.*
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
        modules = arrayOf(
                AndroidInjectionModule::class,
                AndroidSupportInjectionModule::class,
                Supermerk2AppModule::class,
                ActivityModule::class,
                ProductosDataModule::class,
                CategoriaDataModule::class,
                ProveedorDataModule::class,
                AgregarProductosModule::class,
                ThreadModule::class,
                DataBaseModule::class
        )
)
interface Supermerk2Component : AndroidInjector<Supermerk2Application> {
    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<Supermerk2Application>()

}