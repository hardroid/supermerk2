package cl.hardroid.supermerk2.di.module

import android.arch.persistence.room.Room
import android.content.Context
import cl.hardroid.supermerk2.Supermerk2Application
import cl.hardroid.supermerk2.data.datasource.local.Supermerk2DataBase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DataBaseModule {

    @Provides
    @Singleton
    fun provideDataBase(context: Context): Supermerk2DataBase {
        return Room.databaseBuilder(
                context,
                Supermerk2DataBase::class.java,
                "supermerk2-db").build()
    }
}