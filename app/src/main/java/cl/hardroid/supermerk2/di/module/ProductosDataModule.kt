package cl.hardroid.supermerk2.di.module

import cl.hardroid.supermerk2.data.datasource.ProductoDataSource
import cl.hardroid.supermerk2.data.datasource.local.ProductoLocalDataSource
import cl.hardroid.supermerk2.data.datasource.mapper.ProductoModelToProductoEntity
import cl.hardroid.supermerk2.data.repository.ProductoDataRepository
import cl.hardroid.supermerk2.domain.repository.ProductoRepository
import dagger.Module
import dagger.Provides
import dagger.Reusable

@Module
class ProductosDataModule {

    @Reusable
    @Provides
    fun provideProductoRepository(repository: ProductoDataRepository) : ProductoRepository{
        return repository
    }

    @Reusable
    @Provides
    fun provideDataSource(dataSource: ProductoLocalDataSource) : ProductoDataSource{
        return dataSource
    }
}